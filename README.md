Hey there! :wave:

I'm Manuel, a Staff Support Engineer here at GitLab.

I joined the team in December 2021, after having been a happy user for many years before. A few of those years I also was a customer, administrating the self-hosted GitLab instance at my former job. It was from that role that I had to reach out to GitLab Support myself – and the experience I had was so overwhelmingly positive that I knew this would be something I'd want to try and deliver for other people myself.
